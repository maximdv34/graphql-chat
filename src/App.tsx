import React, {lazy, Suspense} from 'react';
import './styles/App.scss';
import {Routes, Route} from 'react-router-dom';
//import { Registration } from '@components/pages/Registration/Registration';
import {Login} from '@components/pages/Login/Login';
import {AuthProvider} from '@components/AuthProvider';
import {Header} from '@components/Header/Header';
import {Home} from '@components/pages/Home/Home';
//import { Profile } from '@components/Profile/Profile';
import {IsRoomsOpenProvider} from '@components/IsRoomsOpenProvider';
import {AuthGuard} from '@components/AuthGuard';
import {UnauthGuard} from '@components/UnauthGuard';
import {LoadingSpinner} from '@components/LoadingSpinner/LoadingSpinner';

const Registration = lazy(() => import('./components/pages/Registration/Registration').then((module) => ({default: module.Registration})));
const Profile = lazy(() => import('./components/pages/Profile/Profile').then((module) => ({default: module.Profile})));
function App() {
  return (
    <AuthProvider>
      <IsRoomsOpenProvider>
        <Suspense fallback={<LoadingSpinner/>}>
          <Header/>
          <Routes>
            <Route path='/Registration' element={<UnauthGuard><Registration/></UnauthGuard>}/>
            <Route path='/Login' element={<UnauthGuard><Login/></UnauthGuard>}/>
            <Route path='/Profile' element={<AuthGuard><Profile/></AuthGuard>}/>
            <Route path='/' element={<AuthGuard><Home/></AuthGuard>}/>
          </Routes>
        </Suspense>
      </IsRoomsOpenProvider>
    </AuthProvider>
  );
}

export default App;
