import {gql} from '@apollo/client';
import {User} from '@typesTS/User';
import {USER_FIELDS} from './fragments';
export interface ResponseDataRegister {
  registration: {user: User; token: string};
}
export const REGISTER_USER = gql`
  mutation registration ($avatar:String!, $email: String!, $password: String!, $login: String!) {
    registration(avatar: $avatar, email: $email, password: $password, login: $login) {
      token
      user {
        ${USER_FIELDS}
      }
    }
  }
`;