export const USER_FIELDS = `
  id
  login
  email 
  avatar
`;
export const MESSAGES = `
  id
  description
  userId
  convId
  date
  user {
    ${USER_FIELDS}
  }
`;
export const CONVERSATION = `
  id
  createdBy
  name
  date
`;
export const TYPING_FIELDS = `
  user {
    ${USER_FIELDS}
  }
`;