import {gql} from '@apollo/client';
import {IMessage} from '@typesTS/Message';
import {MESSAGES} from './fragments';

export interface RepsponseGetAllMessages {
  getAllMessages: IMessage[];
}
export const GET_ALL_MESSAGES = gql`
  query getAllMessages($convId: Int) {
    getAllMessages(convId: $convId) {
      ${MESSAGES}
    }
  }
`;