import {gql} from '@apollo/client';
import {User} from '@typesTS/User';
import {USER_FIELDS} from './fragments';

export interface ResponseDataLogin {
  signIn: {user: User; token: string};

}
export const LOGIN_USER = gql`
  query login ($password: String!, $email: String!) {
    signIn(password: $password, email: $email) {
      token
      user {
        ${USER_FIELDS}
      }
    }
  }
`;