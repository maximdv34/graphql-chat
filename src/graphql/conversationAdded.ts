import {gql} from '@apollo/client';
import {Conversation} from '@typesTS/Conversation';
import {CONVERSATION} from './fragments';

export interface RepsponseDataConversationAdded {
    conversationAdded: Conversation[];
 }

export const CONVERSATION_ADDED = gql`
    subscription conversationAdded {
        conversationAdded {
            ${CONVERSATION}
        }
    }
`;