import {gql} from '@apollo/client';
import {Conversation} from '@typesTS/Conversation';
import {CONVERSATION} from './fragments';

export interface RepsponseDataGetAllConversations {
    getAllConversations: Conversation[];
 }

export const GET_ALL_CONVERSATIONS = gql`
    query getAllConversations {
        getAllConversations {
            ${CONVERSATION}
        }
    }
`;