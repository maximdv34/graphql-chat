import {TypingUser} from '@typesTS/typingUser';
import {gql} from '@apollo/client';
import {TYPING_FIELDS} from './fragments';

export interface RepsponseIsTyping {
    isTyping: TypingUser[];
  }
export const IS_TYPING = gql`
  subscription isTyping($convId: Float!) {
    isTyping(convId: $convId) {
        ${TYPING_FIELDS}
    }
  }
`;