import {TypingUser} from '@typesTS/typingUser';
import {TYPING_FIELDS} from './fragments';
import {gql} from '@apollo/client';

export interface RepsponseTypingUser {
  typingUser: TypingUser[];
}
export const TYPING_USER = gql`
  query typingUser($convId: Int!) {
    typingUser(convId: $convId) {
      ${TYPING_FIELDS}
    }
  }
`;