import {gql} from '@apollo/client';
import {IMessage} from '@typesTS/Message';
import {MESSAGES} from './fragments';

export interface ResponseMessageAdded {
  messageAdded: IMessage[];
}
export const MESSAGES_ADDED = gql`
  subscription messageAdded($date: DateTime!) {
    messageAdded(date: $date) {
      ${MESSAGES}
    }
  }
`;