import {CONVERSATION} from './fragments';
import {gql} from '@apollo/client';
import {Conversation} from '@typesTS/Conversation';

export interface RepsponseDataCreateConversation {
    createConversation: Conversation;
 }

export const CREATE_CONVERSATION = gql`
  mutation createConversation($name: String!) {
    createConversation(name: $name){
        ${CONVERSATION}
    }
  }
`;