import {gql} from '@apollo/client';

export interface RepsponseGetAllMessageStatistics {
    getMessageStatistics: {
        count: number;
        date: string;
    }[];
}
export const GET_ALL_MESSAGE_STATISTICS = gql`
  query getMessageStatistics {
    getMessageStatistics {
      count
      date
    }
  }
`;