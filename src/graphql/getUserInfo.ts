import {gql} from '@apollo/client';
import {User} from '@typesTS/User';
import {USER_FIELDS} from './fragments';
export interface RepsponseDataUserInfo {
  me: {user: User};
 }
export const GET_USER_INFO = gql`
  query userInfo {
    me {
      user {
        ${USER_FIELDS}
      }
    }
  }
`;