import {gql} from '@apollo/client';
import {User} from '@typesTS/User';
import {USER_FIELDS} from './fragments';
export interface RepsponseDataCreateMessage {
  user: User;
 }
export const CREATE_MESSAGE = gql`
  mutation createMessage($convId: Int, $description: String!) {
    createMessage(convId: $convId, description: $description) {
      user {
        ${USER_FIELDS}
      }
    }
  }
`;