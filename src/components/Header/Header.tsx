import React, {useState} from 'react';
import styles from './Header.module.scss';
import {Link} from 'react-router-dom';
import {useAuth} from '@components/AuthProvider';
import {Avatar} from '@components/Avatar/Avatar';
import {DropDawnMenuUser} from '@components/DropDawnMenuUser/DropDawnMenuUser';
import {IconMenu} from '@components/icons/IconMenu';
import {useIsRoomsOpen} from '@components/IsRoomsOpenProvider';

export const  Header: React.FC = () => {
  const {user} = useAuth();
  const {setReverseIsOpen} = useIsRoomsOpen();
  const [showModal, setShowModal] = useState(false);
  const setReverseShowModal = () => {setShowModal(!showModal);};
  return (
    <>
      <header className={styles.header}>
        <div className={styles.menuBlock}>
          { user
          && <div
            className={styles.buttonMenu}
            onClick={setReverseIsOpen}
          >
            <IconMenu/>
          </div>
          }
          <Link to='/'><div className={styles.title}>GQL Chat</div></Link>
        </div>
        {user
          ? <div className={styles.avatar} onClick={setReverseShowModal}>
            <Avatar size='small' alt={user.login[0].toUpperCase()} src={user.avatar}/>
          </div>
          : <Link to='/Login'><div className={styles.login}>Login</div></Link>
        }
        {showModal && <DropDawnMenuUser toggling={setReverseShowModal}/>}
      </header>
    </>
  );
};