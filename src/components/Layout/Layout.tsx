import React, {PropsWithChildren} from 'react';
import styles from './Layout.module.scss';

export const Layout: React.FC<PropsWithChildren> = (props) => {
  return (
    <>
      <div className={styles.container}>
        <div className={styles.layout}>
          {props.children}
        </div>
      </div>
    </>
  );
};