import React, {MouseEventHandler, PropsWithChildren} from 'react';
import styles from './BlockModal.module.scss';
import {createPortal} from 'react-dom';

interface Props extends PropsWithChildren {
    toggling: MouseEventHandler;
}
export const BlockModal: React.FC<Props> = ({toggling, children}) => {
  return (
    createPortal(
      <div className={styles.dialog} onClick={toggling}>
        <div className={styles.modal}
          onClick={(event) => {event.stopPropagation();}
          }>
          {children}
        </div>
      </div>,
      document.body)
  );
};