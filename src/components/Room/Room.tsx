import React, {MouseEventHandler, PropsWithChildren} from 'react';
import styles from './Room.module.scss';
import clsx from 'clsx';

interface Props extends PropsWithChildren {
    checked: boolean;
    click: MouseEventHandler;
}

export const  Room: React.FC<Props> = ({children, checked, click}) => {
  return (
    <>
      <div className={styles.room} onClick={click}>
        <div className={clsx(styles.checkLine, checked ? styles.checkedLine : '')}/>
        <div className={styles.title}>{children}</div>
      </div>
    </>
  );
};