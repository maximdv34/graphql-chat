import React, {useEffect, useState} from 'react';
import styles from './Rooms.module.scss';
import {useAuth} from '@components/AuthProvider';
import {useMutation, useQuery, useSubscription} from '@apollo/client';
import {IconPlus} from '@components/icons/IconPlus';
import {Input} from '@components/Input/Input';
import {Button} from '@components/Button/Button';
import {Room} from '@components/Room/Room';
import {CREATE_CONVERSATION, RepsponseDataCreateConversation} from '@graphql/createConversation';
import {GET_ALL_CONVERSATIONS, RepsponseDataGetAllConversations} from '@graphql/getAllConversations';
import {CONVERSATION_ADDED, RepsponseDataConversationAdded} from '@graphql/conversationAdded';
import {Conversation} from '@typesTS/Conversation';
import {useIsRoomsOpen} from '@components/IsRoomsOpenProvider';
import clsx from 'clsx';

interface Props {
    convId: number;
    setConvId: Function;
}

export const  Rooms: React.FC<Props> = ({convId, setConvId}) => {
  const {user} = useAuth();
  const {isOpen, setReverseIsOpen} = useIsRoomsOpen();
  const [conversations, setConversations] = useState<Conversation[]>([]);
  const [conversationName, setConversationName] = useState('');
  const [isCreateRoom, setIsCreateRoom] = useState(false);
  const {
    data: dataConversations,
    loading: loadingConversations,
    refetch: refetchConversations,
  } = useQuery<RepsponseDataGetAllConversations>(GET_ALL_CONVERSATIONS);
  const [
    createConversation,
  ] = useMutation<RepsponseDataCreateConversation>(
    CREATE_CONVERSATION
  );
  const {
    data: newConversations,
  } = useSubscription<RepsponseDataConversationAdded>(
    CONVERSATION_ADDED
  );

  async function sendConversation() {
    if (conversationName) {
      const response = await createConversation(
        {variables: {name: conversationName}}
      );
      if (response.data) {
        setConversationName('');
      }
    }
  }
  function changeRoom(roomId: number){
    setConvId(roomId);
    setReverseIsOpen();
  }

  useEffect(() => {
    function loadDataConversations() {
      if (!loadingConversations) {
        setConversations(dataConversations?.getAllConversations!);
      }
    }
    loadDataConversations();
  }, [dataConversations?.getAllConversations, user?.id]);

  useEffect(() => {
    function loadDataConversationsAdded() {
      if (newConversations?.conversationAdded) {
        setConversations(newConversations.conversationAdded);
      }
    }
    loadDataConversationsAdded();
  }, [newConversations?.conversationAdded]);

  useEffect(() => {
    function loadConversations() {
      refetchConversations();
    }
    loadConversations();
  }, [user?.id]);

  return (
    <div className={clsx(styles.blockRooms, isOpen ? styles.blockRoomsMobile : '')}>
      <div className={styles.blockTitle}>
        <div className={styles.title}>
          <div>Rooms</div>
          <IconPlus click={() => {setIsCreateRoom(!isCreateRoom);}}/>
        </div>
        {isCreateRoom && (
          <div className={styles.addRoom}>
            <Input type='text'
              onChange={
                (e: React.ChangeEvent<HTMLInputElement>) =>
                  setConversationName(e.target.value)
              }
              value={conversationName}>
                            input
            </Input>
            <div className={styles.button}>
              <Button type='button' action={sendConversation}>Add</Button>
            </div>
          </div>
        )}
        <div className={styles.horizontalLine}/>
      </div>
      <Room
        checked={convId === 0}
        key={0} click={() => changeRoom(0)}>
                General
      </Room>
      {conversations.map((conversation) =>
        <Room
          checked={convId === Number(conversation.id)}
          key={conversation.id}
          click={() => {changeRoom(Number(conversation.id));}}
        >
          {conversation.name}
        </Room>
      )}
    </div>
  );
};
