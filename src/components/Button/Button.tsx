import React, {MouseEventHandler, PropsWithChildren} from 'react';
import styles from './Button.module.scss';
import clsx from 'clsx';

interface Props extends PropsWithChildren {
    type: 'submit' | 'reset' | 'button';
    color?: 'primary' | 'secondary';
    disabled?: boolean;
    action?: MouseEventHandler;
}
export const Button: React.FC<Props> = (
  {type, color, disabled, action, children}) => {
  return (
    <>
      <button
        className={clsx(styles.ButtonMain, color === 'secondary' &&  styles.ButtonMainSecondary)}
        type={type}
        disabled={disabled}
        onClick={action}
      >
        {children}
      </button>
    </>
  );
};
