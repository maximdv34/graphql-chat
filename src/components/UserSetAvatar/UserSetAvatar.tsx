import React, {useState} from 'react';
import styles from './UserSetAvatar.module.scss';
import {Avatar} from '@components/Avatar/Avatar';
import {Input} from '@components/Input/Input';

interface Props {
    formContext: JSX.IntrinsicAttributes
    & React.ClassAttributes<HTMLInputElement>
    & React.InputHTMLAttributes<HTMLInputElement>;
    alt: string;
}
export const UserSetAvatar: React.FC<Props> = ({formContext, alt}) => {
  const [logo, setLogo] = useState('');
  return (
    <>
      <div className={styles.blockRectangle}>
        <div className={styles.subtitle}>Logo</div>
        <div className={styles.rectangle}>
          <Avatar size='big' alt={alt} src={logo}/>
          <Input
            type="text"
            formContext={formContext}
            onChange={
              (e: React.ChangeEvent<HTMLInputElement>) =>
                setLogo(e.target.value)
            }
            value={logo}>url</Input>
        </div>
      </div>
    </>
  );
};
