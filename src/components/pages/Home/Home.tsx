import {Layout} from '@components/Layout/Layout';
import React, {useState} from 'react';
import styles from './Home.module.scss';
import {Rooms} from '@components/Rooms/Rooms';
import {Messages} from '@components/Messages/Messages';

export const  Home: React.FC = () => {
  const [convId, setConvId] = useState(0);
  return (
    <Layout>
      <div className={styles.home}>
        <Rooms convId={convId} setConvId={setConvId}/>
        <Messages convId={convId}/>
      </div>
    </Layout>
  );
};
