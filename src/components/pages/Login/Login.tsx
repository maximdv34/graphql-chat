import React, {useState} from 'react';
import {useForm} from 'react-hook-form';
import {useMutation} from '@tanstack/react-query';
import {useLazyQuery} from '@apollo/client';
import * as yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import {Button} from '@components/Button/Button';
import {Input} from '@components/Input/Input';
import {Layout} from '@components/Layout/Layout';
import styles from './Login.module.scss';
import {Link, useNavigate} from 'react-router-dom';
import {useAuth} from '../../AuthProvider';
import {LOGIN_USER, ResponseDataLogin} from '@graphql/loginUser';
import {wsClient} from '@constants';

export const Login: React.FC = () => {
  const navigate = useNavigate();
  const [error, setError] = useState('');
  const [isWaiting, setIsWaiting] = useState(false);
  const {setDataUser} = useAuth();
  const [loginUser] = useLazyQuery<ResponseDataLogin>(LOGIN_USER);
    interface InputTypes {
        login: string;
        password: string;
      }
    const validationSchema = yup
      .object()
      .shape({
        login: yup.string().email()
          .required(),
        password: yup.string()
          .required(),
      });
    const {
      register,
      handleSubmit,
      formState: {errors},
    } = useForm<InputTypes>({resolver: yupResolver(validationSchema)});
    async function signIn(data: InputTypes) {
      const response = loginUser({variables: {
        email: data.login,
        password: data.password,
      }});
      return response;
    }
    const {mutateAsync} = useMutation({mutationFn: signIn});
    async function submitForm(data: InputTypes) {
      setIsWaiting(true);
      try {
        const response = await mutateAsync(data);
        if (response.data?.signIn.token) {
          localStorage.setItem('token', response.data.signIn.token);
          setDataUser(response.data.signIn.user);
          wsClient.close(false);
          navigate('/', {replace: true});
        }
        else if (response.error?.message) {
          setError(String(response.error.message));
        }
      } catch (e: any) {
        console.error(`[SERVER]: ${e.message}`);
      }
      setIsWaiting(false);
    }
    return (
      <>
        <Layout>
          <div className={styles.ContainerLogin}>
            <form
              className={styles.form}
              onSubmit={handleSubmit((data) =>
                submitForm(data))}
            >
              <div className={styles.title}>Welcome</div>
              <Input type="email" formContext={register('login')} error={errors.login?.message}>Login</Input>
              <Input type="password" formContext={register('password')} error={errors.password?.message}>Password</Input>
              {error && (<div className="error">{error}</div>)}
              <div className={styles.blockButton}>
                <Link to='/Registration' className={styles.link}>Registration</Link>
                <Button type='submit' disabled={isWaiting}>Login</Button>
              </div>
            </form>
          </div>
        </Layout>
      </>
    );
};