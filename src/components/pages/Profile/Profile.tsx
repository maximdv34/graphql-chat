import {useQuery} from '@apollo/client';
import {useAuth} from '@components/AuthProvider';
import {Avatar} from '@components/Avatar/Avatar';
import {BarForStatistic} from '@components/BarForStatistic';
import {Button} from '@components/Button/Button';
import {Layout} from '@components/Layout/Layout';
import {GET_ALL_MESSAGE_STATISTICS, RepsponseGetAllMessageStatistics} from '@graphql/getMessageStatistics';
import React, {useEffect} from 'react';
import {getFormatedDate} from '@utils/getFormatedDate';
import styles from './Profile.module.scss';

export const  Profile: React.FC = () => {
  const {user, clearDataUser} = useAuth();
  const token = localStorage.getItem('token');
  const decoded_token = JSON.parse(atob(token!.split('.')[1]));
  const expared_date = new Date(decoded_token.exp * 1000);
  const formated_date = getFormatedDate(expared_date);
  const {data, loading, refetch} = useQuery<RepsponseGetAllMessageStatistics>(
    GET_ALL_MESSAGE_STATISTICS
  );

  useEffect(() => {
    refetch();
  }, [user?.id]);

  return (
    <Layout>
      <div className={styles.profile}>
        <div className={styles.mainTitle}>Profile</div>
        { user && (
          <>
            <div className={styles.blockUserData}>
              <div>
                <Avatar size='big' alt={user.login[0].toUpperCase()} src={user.avatar}/>
              </div>
              <div className={styles.userData}>
                <div className={styles.login}>{user.login}</div>
                <div className={styles.email}>{user.email}</div>
                <div className={styles.expairAt}>
                            Token expair at:<br/>
                  {formated_date}
                </div>
              </div>
            </div>
            <div className={styles.blockMessageStatistics}>
              <div className={styles.title}>Your message statistic</div>
              {!loading
              && <BarForStatistic data={data?.getMessageStatistics!}
              />}
              <div className={styles.button}><Button type='button' action={clearDataUser}>Logout</Button></div>
            </div>
          </>
        )}
      </div>
    </Layout>
  );
};
