import React, {useState} from 'react';
import {useForm} from 'react-hook-form';
import {useMutation} from '@tanstack/react-query';
import {useMutation as useMutationGql} from '@apollo/client';
import * as yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import {Button} from '@components/Button/Button';
import {Input} from '@components/Input/Input';
import {Layout} from '@components/Layout/Layout';
import styles from './Registration.module.scss';
import {REGISTER_USER, ResponseDataRegister} from '@graphql/registerUser';
import {useNavigate} from 'react-router-dom';
import {UserSetAvatar} from '@components/UserSetAvatar/UserSetAvatar';

export const  Registration: React.FC = () => {
  const navigate = useNavigate();
  const [error, setError] = useState(false);
  const [isWaiting, setIsWaiting] = useState(false);
  const [loginInput, setLoginInput] = useState('');
  const [addUser] = useMutationGql<ResponseDataRegister>(REGISTER_USER);
    interface InputTypes {
        login: string;
        email: string;
        password: string;
        repeat: string;
        avatar: string;
      }
    const validationSchema = yup
      .object()
      .shape({
        login: yup.string()
          .required(),
        email: yup.string()
          .email()
          .required(),
        password: yup.string()
          .required(),
        repeat: yup.string()
          .required()
          .oneOf([yup.ref('password')], 'passwords do not match'),
        avatar: yup.string(),
      });
    const {
      register,
      handleSubmit,
      formState: {errors},
    } = useForm<InputTypes>({resolver: yupResolver(validationSchema)});
    async function signUp(data: InputTypes){
      const response = addUser({variables: {
        avatar: data.avatar,
        email: data.email,
        password: data.password,
        login: data.login,
      }});
      return response;
    }
    const {mutateAsync} = useMutation({mutationFn: signUp});
    async function submitForm(data: InputTypes) {
      setIsWaiting(true);
      try {
        const response = await mutateAsync(data);
        if (response.data) {
          navigate('/Login', {replace: true});
        }
      } catch (e: any) {
        console.error(`[SERVER]: ${e.message}`);
        setError(e.message);
      }
      setIsWaiting(false);
    }
    return (
      <>
        <Layout>
          <div className={styles.ContainerRegistration}>
            <form
              className={styles.form}
              onSubmit={handleSubmit((data) => submitForm(data))}
            >
              <div className={styles.title}>Registration</div>
              <Input type="text"
                formContext={register('login')}
                error={errors.login?.message}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  setLoginInput(e.target.value)}
                value={loginInput}>
                Login
              </Input>
              <Input type="email" formContext={register('email')} error={errors.email?.message}>Email</Input>
              <Input type="password" formContext={register('password')} error={errors.password?.message}>Password</Input>
              <Input type="password" formContext={register('repeat')} error={errors.repeat?.message}>Repeat password</Input>
              <UserSetAvatar formContext={register('avatar')} alt={loginInput ? loginInput[0].toUpperCase() : ' '}/>
              {error && (<div className="error">{error}</div>)}
              <div className={styles.blockButton}>
                <Button type='submit' disabled={isWaiting}>Register</Button>
              </div>
            </form>
          </div>
        </Layout>
      </>
    );
};