import {Message} from '@components/Message/Message';
import React, {useEffect, useState} from 'react';
import styles from './Messages.module.scss';
import {useAuth} from '@components/AuthProvider';
import {useLazyQuery, useMutation, useQuery, useSubscription} from '@apollo/client';
import {CREATE_MESSAGE, RepsponseDataCreateMessage} from '@graphql/createMessage';
import {MESSAGES_ADDED, ResponseMessageAdded} from '@graphql/messageAdded';
import {GET_ALL_MESSAGES, RepsponseGetAllMessages} from '@graphql/getAllMessages';
import {IMessage} from '@typesTS/Message';
import {IconPaperAirplane} from '@components/icons/IconPaperAirplane';
import {CURRENT_DATE} from '@constants';
import {TypingUsers} from '@components/TypingUsers/TypingUsers';
import {useDebouncedCallback} from 'use-debounce';
import {RepsponseTypingUser, TYPING_USER} from '@graphql/typingUser';

interface Props {
    convId: number;
}

export const Messages: React.FC<Props> = ({convId}) => {
  const {user} = useAuth();
  const [messages, setMessages] = useState<IMessage[]>([]);
  const [messageText, setMessageText] = useState('');
  const {
    data,
    loading: loadingMessages,
    refetch: refetchMessages,
  } = useQuery<RepsponseGetAllMessages>(
    GET_ALL_MESSAGES,
    {variables: {convId}}
  );
  const [createMessage] = useMutation<RepsponseDataCreateMessage>(
    CREATE_MESSAGE
  );
  const {data: updatedData} = useSubscription<ResponseMessageAdded>(
    MESSAGES_ADDED,
    {variables: {date: CURRENT_DATE}}
  );
  const [, {refetch: refetchTypingUser}] = useLazyQuery<RepsponseTypingUser>(
    TYPING_USER,
    {variables: {convId}}
  );
  const debounced = useDebouncedCallback(
    () => {
      if (messageText) {
        refetchTypingUser();
      }
    },
    1000
  );
  async function sendMessage() {
    if (messageText) {
      const response = await createMessage(
        {variables: {convId, description: messageText}}
      );
      if (response.data) {
        setMessageText('');
      }
    }
  }
  function handleChangeTextArea(e: React.ChangeEvent<HTMLTextAreaElement>) {
    setMessageText(e.target.value);
    debounced();
  }

  useEffect(() => {
    function loadDataMessages() {
      if (!loadingMessages) {
        setMessages(data?.getAllMessages.slice(0).reverse()!);
      }
    }
    loadDataMessages();
  }, [data?.getAllMessages, user?.id]);
  useEffect(() => {
    function loadDataMessagesAdded() {
      if (updatedData?.messageAdded) {
        setMessages([...updatedData.messageAdded, ...messages]);
      }
    }
    loadDataMessagesAdded();
  }, [updatedData?.messageAdded]);
  useEffect(() => {
    function loadMessages() {
      refetchMessages();
      setMessageText('');
    }
    loadMessages();
  }, [convId]);
  return (
    <div className={styles.blockMessages}>
      <div className={styles.messages}> {/* TODO test this block in all brawsers */}
        { messages.map((message) =>
          <Message message={message} direction={user?.id === message.user.id ? 'right' : 'left'} key={message.id}/>
        )}
      </div>
      <TypingUsers convId={convId}/>
      <div className={styles.blockInputMessage}>
        <textarea
          className={styles.textarea}
          onChange={handleChangeTextArea}
          value={messageText}
        />
        <div className={styles.button} onClick={sendMessage}>
          <IconPaperAirplane/>
        </div>
      </div>
    </div>
  );
};
