import React from 'react';

export const IconMenu: React.FC = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24" height="21">
      <path xmlns="http://www.w3.org/2000/svg" d="M0 0L24 0L24 5L0 5L0 0Z" fill="#d8d8d8"/>
      <path xmlns="http://www.w3.org/2000/svg" d="M0 0L24 0L24 5L0 5L0 0Z" fill="#d8d8d8" transform="translate(0, 8)"/>
      <path xmlns="http://www.w3.org/2000/svg" d="M0 0L24 0L24 5L0 5L0 0Z" fill="#d8d8d8" transform="translate(0, 16)"/>
    </svg>
  );
};