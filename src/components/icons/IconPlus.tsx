import React, {MouseEventHandler} from 'react';
interface Props {
    click?: MouseEventHandler;
}
export const IconPlus: React.FC<Props> = ({click}) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="16" height="16" onClick={click}>
      <path d="M236.235 -50.5098L231.529 -50.5098L231.529 -44.8627L225.882 -44.8627L225.882 -40.1569L231.529 -40.1569L231.529 -34.5098L236.235 -34.5098L236.235 -40.1569L241.882 -40.1569L241.882 -44.8627L236.235 -44.8627L236.235 -50.5098Z" fillRule="evenodd" fill="#6a6868" transform="translate(-225.88235473632812, 50.509803771972656)"/>
    </svg>
  );
};