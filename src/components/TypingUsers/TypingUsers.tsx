import React from 'react';
import styles from './TypingUsers.module.scss';
import {useSubscription} from '@apollo/client';
import {IS_TYPING, RepsponseIsTyping} from '@graphql/isTyping';
import {useAuth} from '@components/AuthProvider';

interface Props {
    convId: number;
}

export const TypingUsers: React.FC<Props> = ({convId}) => {
  const {user} = useAuth();
  const {data: dataTypingUsers} = useSubscription<RepsponseIsTyping>(
    IS_TYPING,
    {variables: {convId}}
  );
  return (
    <div className={styles.typingUser}>
      {dataTypingUsers?.isTyping && dataTypingUsers.isTyping.length > 0 && (
        <>
          {dataTypingUsers.isTyping.map((u) =>
            user?.id !== u.user.id
                && <div key={u.user.id}>{u.user.login}&nbsp;</div>
          )}
          {user
          && dataTypingUsers.isTyping.some(u => u.user.id !== user.id)
          && <div>typing...</div>
          }
        </>
      )}
    </div>
  );
};