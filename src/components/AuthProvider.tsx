import {useApolloClient} from '@apollo/client';
import React, {createContext, useState, useContext, PropsWithChildren, useEffect} from 'react';
import {GET_USER_INFO, RepsponseDataUserInfo} from '@graphql/getUserInfo';
import {User} from '@typesTS/User';
import {useNavigate} from 'react-router-dom';

interface AuthContextValue {
  user: User | null;
  setDataUser: (user: User) => void;
  clearDataUser: () => void;
  isReady: boolean;
}

const AuthContext = createContext<AuthContextValue>({
  user: null,
  setDataUser: () => {},
  clearDataUser: () => {},
  isReady: false,
});

export const AuthProvider: React.FC<PropsWithChildren> = ({children}) => {
  const [user, setUser] = useState<User | null>(null);
  const [isReady, setIsReady] = useState<boolean>(false);
  const client = useApolloClient();
  const navigate = useNavigate();

  const setDataUser = (userData: User) => {
    setUser(userData);
  };
  const clearDataUser = () => {
    setUser(null);
    localStorage.removeItem('token');
    navigate('/Login', {replace: true});
  };

  const contextValue: AuthContextValue = {
    user,
    setDataUser,
    clearDataUser,
    isReady,
  };

  useEffect(() => {
    async function signIn(){
      setIsReady(false);
      if (localStorage.getItem('token')) {
        const response = await client.query<RepsponseDataUserInfo>(
          {query: GET_USER_INFO}
        );
        if (response.data.me.user) {
          setUser(response.data.me.user);
        } else {
          clearDataUser();
        }
      } else {
        navigate('/Login', {replace: true});
      }
      setIsReady(true);
    }
    signIn();
  }, []);

  return (
    <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>
  );
};

export const useAuth = (): AuthContextValue => {
  const authContext = useContext(AuthContext);

  if (!authContext) {
    throw new Error('useAuth must be used within an AuthProvider');
  }

  return authContext;
};