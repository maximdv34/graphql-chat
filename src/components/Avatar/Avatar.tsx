import React, {useEffect, useState} from 'react';
import styles from './Avatar.module.scss';
import clsx from 'clsx';
interface Props{
    size: 'small' | 'big';
    src?: string;
    alt: string;
}
export const Avatar: React.FC<Props> = ({size, src, alt}) => {
  const [isExist, setIsExist] = useState(false);
  useEffect(() => {
    if (src){
      const img = new Image();
      img.src = src;
      img.onload = function () {
        setIsExist(true);
      };
    }
  }, []);
  return (
    <>
      <div className={clsx(styles.blockAvatar, size === 'small' ? styles.blockAvatarSmall : styles.blockAvatarBig)}>
        { src && isExist ? <img src={src} alt={alt}/> : <span>{alt}</span> }
      </div>
    </>
  );
};