import React, {createContext, useState, useContext, PropsWithChildren} from 'react';

interface isRooomsContextValue {
  isOpen: boolean;
  setReverseIsOpen: () => void;
}

const isRoomsContext = createContext<isRooomsContextValue>({
  isOpen: false,
  setReverseIsOpen: () => {},
});

export const IsRoomsOpenProvider: React.FC<PropsWithChildren> = (
  {children}
) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const setReverseIsOpen = () => {setIsOpen(!isOpen);};
  const contextValue: isRooomsContextValue = {
    isOpen,
    setReverseIsOpen,
  };
  return (
    <isRoomsContext.Provider
      value={contextValue}>{children}</isRoomsContext.Provider>
  );
};

export const useIsRoomsOpen = (): isRooomsContextValue => {
  const context = useContext(isRoomsContext);

  if (!context) {
    throw new Error('useAuth must be used within an IsRoomsOpenProvider');
  }

  return context;
};