import React, {PropsWithChildren} from 'react';
import styles from './Input.module.scss';
import clsx from 'clsx';
interface Props extends PropsWithChildren {
    type: string;
    placeholder?: string;
    formContext?: JSX.IntrinsicAttributes
    & React.ClassAttributes<HTMLInputElement>
    & React.InputHTMLAttributes<HTMLInputElement>;
    error?: string;
    onChange?: React.ChangeEventHandler<HTMLInputElement>;
    value?: string;
    customClass?: string;
}
export const Input: React.FC<Props> = ({
  type,
  placeholder,
  formContext,
  error,
  children,
  onChange,
  value,
  customClass,
}) => {
  return (
    <>
      <div className={clsx(customClass, styles.blockInput, error ? styles.blockInputError : '')}>
        { children && <div className={styles.text}>{children}</div> }
        <input
          type={type}
          placeholder={placeholder}
          {...formContext}
          onChange={onChange}
          value={value}
        />
        {error && <div className={styles.error}>{error}</div>}
      </div>
    </>
  );
};