import React from 'react';
import styles from './LoadingSpinner.module.scss';
export const LoadingSpinner: React.FC = () => {
  return (
    <div className={styles.container}>
      <div className={styles.loader}>Loading...</div>
    </div>
  );
};