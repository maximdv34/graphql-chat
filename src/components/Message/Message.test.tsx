import React from 'react';
import {unmountComponentAtNode} from 'react-dom';
import {Message} from './Message';
import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom';
let container: any = null;
beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe('Message', () => {
  const message = {
    id: '1',
    description: 'Hello, world!',
    userId: 1,
    convId: 1,
    date: '12.12.2021 11:05',
    user: {
      id: '1',
      login: 'john_doe',
      email: 'devmaksim3@gmail.com',
      avatar: 'https://example.com/avatar.jpg',
    },
  };

  it('renders message description and date', () => {
    render(<Message message={message} direction="left" />, container);
    expect(screen.getByText(message.description)).toBeInTheDocument();
    expect(screen.getByText(message.date)).toBeInTheDocument();
  });

  it('renders message with direction "left"', () => {
    const {getByTestId} = render(<Message message={message} direction="left" />, container);
    expect(getByTestId('message')).toHaveClass('blockMessage', 'blockMessageLeft');
  });

  it('renders message with direction "right"', () => {
    const {getByTestId} = render(<Message message={message} direction="right" />, container);
    expect(getByTestId('message')).toHaveClass('blockMessage', 'blockMessageRight');
  });

  it('renders user avatar with alt text', () => {
    render(<Message message={message} direction="left" />, container);
    const avatar = screen.getByRole('img');
    expect(avatar).toHaveAttribute('alt', message.user.login[0].toUpperCase());
  });
});