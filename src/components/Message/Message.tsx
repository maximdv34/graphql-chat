import React from 'react';
import styles from './Message.module.scss';
//import { Avatar } from '@components/Avatar/Avatar';
import {Avatar} from '../Avatar/Avatar';
import clsx from 'clsx';
import {IMessage} from '@typesTS/Message';
import {getFormatedDate} from '@utils/getFormatedDate';

interface Props{
    message: IMessage;
    direction: 'left' | 'right';
}

export const  Message: React.FC<Props> = ({message, direction}) => {
  const date = getFormatedDate(new Date(message.date));
  return (
    <>
      <div  data-testid="message" className={clsx(styles.blockMessage, direction === 'left' ? styles.blockMessageLeft : styles.blockMessageRight)}>
        <div>
          <Avatar size='small' alt={message.user.login[0].toUpperCase()} src={message.user.avatar}/>
        </div>
        <div>
          <div className={styles.messageText}>
            {message.description}
          </div>
          <div className={styles.date}>
            {date}
          </div>
        </div>
      </div>
    </>
  );
};