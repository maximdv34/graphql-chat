import React, {PropsWithChildren} from 'react';
import {useAuth} from './AuthProvider';
import {Navigate} from 'react-router-dom';
import {LoadingSpinner} from './LoadingSpinner/LoadingSpinner';

export const AuthGuard: React.FC<PropsWithChildren> = ({children}) => {
  const {user, isReady} = useAuth();
  if (user) {
    return (
      <>
        {children}
      </>
    );
  } else if (isReady){
    return (<Navigate to="/Login"/>);
  }
  else {
    return (
      <LoadingSpinner/>
    );
  }

};