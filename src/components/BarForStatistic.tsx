import React from 'react';
import {Bar} from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import {getFormatedDate} from '@utils/getFormatedDate';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

interface Props {
    data: {
        count: number;
        date: string;
    }[];
}

export const BarForStatistic: React.FC<Props> = ({data}) => {
  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top' as const,
      },
    },
  };
  const dates = data.map((m) => {return getFormatedDate(new Date(m.date));});
  const counts = data.map((m) => {return m.count;});
  const state = {
    labels: dates,
    datasets: [
      {
        label: 'Messages',
        backgroundColor: ['#FF0B8450', '#FF983F50', '#00C9C050', '#40B4F150', '#B600FF50', '#C9CBCF50'],
        borderColor: ['#FF0B84', '#FF983F', '#00C9C0', '#40B4F1', '#B600FF', '#C9CBCF'],
        borderWidth: 1,
        data: counts,
      },
    ],
  };
  return <Bar options={options} data={state} />;
};