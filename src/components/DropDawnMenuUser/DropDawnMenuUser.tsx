import React, {MouseEventHandler} from 'react';
import styles from './DropDawnMenuUser.module.scss';
import {useAuth} from '@components/AuthProvider';
import {createPortal} from 'react-dom';
import {BlockModal} from '@components/BlockModal/BlockModal';
import {useNavigate} from 'react-router-dom';

interface Props {
    toggling: MouseEventHandler;
}

export const DropDawnMenuUser: React.FC<Props> = ({toggling}) => {
  const {clearDataUser} = useAuth();
  const navigate = useNavigate();

  function logout(e: any) {
    clearDataUser();
    toggling(e);
  }
  function openProfile(e: any) {
    navigate('/Profile', {replace: true});
    toggling(e);
  }

  return (
    createPortal(
      <BlockModal toggling={toggling}>
        <div className={styles.absoluteBlock}>
          <div onClick={openProfile} className={styles.item}>Profile</div>
          <div className={styles.line}/>
          <div onClick={logout} className={styles.item}>Logout</div>
        </div>
      </BlockModal>,
      document.body)
  );
};