import React, {PropsWithChildren} from 'react';
import {useAuth} from './AuthProvider';
import {Navigate} from 'react-router-dom';
import {LoadingSpinner} from './LoadingSpinner/LoadingSpinner';

export const UnauthGuard: React.FC<PropsWithChildren> = ({children}) => {
  const {user, isReady} = useAuth();
  if (isReady && !user) {
    return (
      <>
        {children}
      </>
    );
  } else if (isReady){
    return (<Navigate to="/"/>);
  }
  else {
    return (
      <LoadingSpinner/>
    );
  }

};