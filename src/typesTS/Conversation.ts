export interface Conversation {
    id: string;
    createdBy: Float32Array;
    name: string;
    date: string;
}