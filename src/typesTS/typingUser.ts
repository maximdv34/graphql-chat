import {User} from './User';

export interface TypingUser {
    user: User;
}