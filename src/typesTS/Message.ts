import {User} from './User';

export interface IMessage {
    id: string;
    description: string;
    userId: number;
    convId: number;
    date: string;
    user: User;
}