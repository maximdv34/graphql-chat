import {ApolloClient, createHttpLink, InMemoryCache, split} from '@apollo/client';
import {setContext} from '@apollo/client/link/context';
import {GraphQLWsLink} from '@apollo/client/link/subscriptions';
import {getMainDefinition} from '@apollo/client/utilities';
import {createClient} from 'graphql-ws';
import {WebSocketLink} from '@apollo/client/link/ws';
import {SubscriptionClient} from 'subscriptions-transport-ws';

export const CURRENT_DATE = new Date('2024-12-31T03:24:00');
export const wsClient = new SubscriptionClient(
  process.env.REACT_APP_WS_SERVER_URI!, {
    connectionParams: () => {
      return {'access-token': localStorage.getItem('token') ?? null, reconnect: true};
    },
  });
const wsLink = new WebSocketLink(
  wsClient
  /*
  new SubscriptionClient(process.env.REACT_APP_WS_SERVER_URI!, {
    connectionParams: () => {
      const token = localStorage.getItem("token");
      return { "access-token": token, reconnect: true,};
  },
  })
  */
);
/*
const wsLink = new GraphQLWsLink(createClient({
  url: `${process.env.REACT_APP_WS_SERVER_URI}`,
  connectionParams: () => {
      debugger
      return { "access-token": localStorage.getItem("token") || null, reconnect: true,};
  },

}));
*/

const httpLink = createHttpLink({
  uri: `${process.env.REACT_APP_SERVER_URI!}`,
});
const authLink = setContext((_, {headers}) => {
  const token = localStorage.getItem('token');
  return {
    headers: {
      ...headers,
      'access-token': token ? `${token}` : '',
    },
  };
});
const splitLink = split(
  ({query}) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition'
      && definition.operation === 'subscription'
    );
  },
  wsLink,
  authLink.concat(httpLink)
);
export const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: splitLink,
});
